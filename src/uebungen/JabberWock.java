package uebungen;

public class JabberWock {
	
	String color;
	String sex;
	boolean hungry;
	
	JabberWock(){
		color = null;
		sex = null;
		hungry = true;
	}
	
	JabberWock(String c, String s){
		color = c;
		sex = s;
		hungry = true;
	}
	
	void feelJabberWock() {
		if (hungry) {
			System.out.println("Yum --- a pleasant!");
			hungry = false;
		}else
			System.out.println("no, thamks --- already ate.");
		System.out.println("***");
	}
	
	void showAttributes() {
		System.out.println("This is a " + sex + " " + color + " jabberwock");
		if (hungry)
			System.out.println("Zhte jabberwock is hungry");
		else
			System.out.println("Zhte jabberwock is full");
		System.out.println("***");
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*JabberWock j = new JabberWock();
		j.color = "orange";
		j.sex = "male";*/
		//j.hungry = true;
		JabberWock j = new JabberWock("ornage","male");
		System.out.println("Calling Attributes ...");
		j.showAttributes();
		System.out.println("Feeding the jabberwock .....");
		j.feelJabberWock();
		System.out.println("Calling Attributes ...");
		j.showAttributes();
		System.out.println("Feeding the jabberwock .....");
		j.feelJabberWock();
	}

}
