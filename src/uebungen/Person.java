package uebungen;

public class Person {
	//Eigenschaften
	String name;
	int age;
	
	//Konstruktoren
	Person(String n, int a){
		name = n;
		age = a;
	}
	//StandartKonstruktor
	Person(){
		name = null;
		age = 0;
	}
	
	// Methoden
	void printPerson() {
		System.out.println("Hi, my name is " + name + " my age is " + age);
		System.out.println("***");
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Person p;
		p = new Person("Luke",30);

	}

}
