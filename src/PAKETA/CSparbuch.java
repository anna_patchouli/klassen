package PAKETA;


public class CSparbuch {
	double kapital;
	double zinssatz;
	
	CSparbuch(double kap,double zins){
		kapital = kap;
		zinssatz = zins;
	}
	
	void einzahlen(double betrag) {
		kapital += betrag;
	}
	
	void abheben(double betrag) {
		kapital -= betrag;
	}
	
	double ertrag1(double laufzeit) {
		return kapital*Math.pow((1 + zinssatz)/100, laufzeit);	
	}
		
	double ertrag(double laufzeit) {
		return kapital * (1 + zinssatz/100*laufzeit);
	}
	

}
